%global commit0 5af314be9e04b9f606eb705b2d70ceb9bf527606
%global shortcommit0 %(c=%{commit0}; echo ${c:0:8})
%global projectname completor.vim

Name: vim-completor
Version: 20201128.git.%{shortcommit0}
Release: 1%{?dist}
Summary: an asynchronous code completion framework for vim8

BuildArch: noarch
License: MIT
Source0: https://github.com/maralla/%{projectname}/archive/%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz
Source1: completor-c.vim
Source2: completor-python.vim
Source3: completor-javascript.vim

Requires: vim

%description
Completor is an asynchronous code completion framework for vim8. New features
of vim8 are used to implement the fast completion engine with low overhead.
For using semantic completion, external completion tools should be installed.

%package -n vim-completor-c
Summary: vim-completor plugin for C/C++ completion
Requires: vim-completor
Requires: clang

%description -n vim-completor-c
Uses clang for C/C++ completion.

%package -n vim-completor-python
Summary: vim-completor plugin for python completion
Requires: vim-completor
Requires: python3-jedi

%description -n vim-completor-python
Uses jedi for python completion.

%package -n vim-completor-javascript
Summary: vim-completor plugin for javascript completion
Requires: vim-completor
Requires: nodejs-tern

%description -n vim-completor-javascript
Uses nodejs tern for javascript completion.

%prep
%setup -q -n %{projectname}-%{commit0}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_datadir}/vim/vimfiles/
cp -r autoload %{buildroot}%{_datadir}/vim/vimfiles/
cp -r doc %{buildroot}%{_datadir}/vim/vimfiles/
cp -r plugin %{buildroot}%{_datadir}/vim/vimfiles/
cp -r pythonx %{buildroot}%{_datadir}/vim/vimfiles/

mkdir -p %{buildroot}%{_datadir}/doc/%{name}
cp README.md %{buildroot}%{_datadir}/doc/%{name}/

mkdir -p %{buildroot}%{_datadir}/licenses/%{name}
cp LICENSE %{buildroot}%{_datadir}/licenses/%{name}/

rm -rf tests

install -Dm0644 %{SOURCE1} %{buildroot}%{_datadir}/vim/vimfiles/plugin/completor-c.vim
install -Dm0644 %{SOURCE2} %{buildroot}%{_datadir}/vim/vimfiles/plugin/completor-python.vim
install -Dm0644 %{SOURCE3} %{buildroot}%{_datadir}/vim/vimfiles/plugin/completor-javascript.vim

%files
%license LICENSE
%doc README.md
%dir %{_datadir}/vim/vimfiles/autoload/completor
%dir %{_datadir}/vim/vimfiles/pythonx
%dir %{_datadir}/vim/vimfiles/pythonx/completers
%dir %{_datadir}/vim/vimfiles/pythonx/completor
%{_datadir}/vim/vimfiles/autoload/completor.vim
%{_datadir}/vim/vimfiles/autoload/completor/*
%{_datadir}/vim/vimfiles/doc/completor.txt
%{_datadir}/vim/vimfiles/plugin/completor.vim
%{_datadir}/vim/vimfiles/pythonx/completers/*
%{_datadir}/vim/vimfiles/pythonx/completor/*

%files -n vim-completor-c
%{_datadir}/vim/vimfiles/plugin/completor-c.vim

%files -n vim-completor-python
%{_datadir}/vim/vimfiles/plugin/completor-python.vim

%files -n vim-completor-javascript
%{_datadir}/vim/vimfiles/plugin/completor-javascript.vim

%changelog

